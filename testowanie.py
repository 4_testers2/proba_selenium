
# def test_logout_correctly(browser):
# # Przykładowa asercja - sprawdzenie że na stronie jest jakiś selektor który wyświetla
# # się tylko w przypadku udanego logowania
#     assert browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]').is_displayed() is True
#
#     # Zamknięcie przeglądarki
#     # browser.quit()

# def test_add_project(browser):
#     envelope = browser.find_element(By.CSS_SELECTOR, '.top_messages')
#     envelope.click()
#     wait = WebDriverWait(browser, 10)
#     selector = (By.CSS_SELECTOR,  '.j_msgResponse')
#     panel_with_messages = wait.until(EC.element_to_be_clickable(selector))
#     assert panel_with_messages.is_displayed()

# def test_open_administration(browser):
#     admin_button = browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
#     admin_button.click()
#     # wait = WebDriverWait(browser, 10)
#     # selector = (By.CSS_SELECTOR,  '.j_msgResponse')
#     # panel_with_messages = wait.until(EC.element_to_be_clickable(selector))
#     assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'

# def test_login_with_elements_as_objects():
#     # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
#     browser = Chrome(executable_path=ChromeDriverManager().install())
#
#     # Otwarcie strony testareny - pierwsze użycie Selenium API
#     browser.get('http://demo.testarena.pl/zaloguj')
#
#     # Stworzenie obiektów reprezentujących elementy strony
#     email_input = browser.find_element(By.CSS_SELECTOR, "#email")
#     password_input = browser.find_element(By.CSS_SELECTOR, "#password")
#     login_button = browser.find_element(By.CSS_SELECTOR, "#login")
#
#     # Wpisanie login, hasła i kliknięcie login
#     email_input.send_keys("administrator@testarena.pl")
#     password_input.send_keys("sumXQQ72$L")
#     login_button.click()
#
#     # Przykładowa asercja - sprawdzenie że na stronie jest jakiś selektor który wyświetla
#     # się tylko w przypadku udanego logowania
#     log_out_button = browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]')
#     assert log_out_button.is_displayed() is True

# @pytest.fixture()
# def browser():
#     browser = Chrome(executable_path=ChromeDriverManager().install())
#     # stworzenie obiektu klasy (page objectu) LoginPage'a
#     login_page = LoginPage(browser)
#     # wywołanie metod na obiekcie klasy
#     login_page.load()
#     login_page.login("administrator@testarena.pl", "sumXQQ72$L")
#     yield browser
#     browser.quit()
#
#
# def test_logout_correctly_displayed(browser):
#     assert browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]').is_displayed() is True
#
#
# def test_opens_messages(browser):
#     cockpit_page = CockpitPage(browser)
#     cockpit_page.click_envelope()
#
#     panel_page = PanelPage(browser)
#     panel_with_messages = panel_page.wait_for_load()
#     assert panel_with_messages.is_displayed()
#
#
# def test_open_administration(browser):
#     cockpit_page = CockpitPage(browser)
#     cockpit_page.click_administration()
#
#     assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'
