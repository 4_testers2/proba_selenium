import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from faker import Faker
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support import expected_conditions as EC
from pages.login_page import LoginPage
from pages.project_page import ProjectPage
from pages.navigation_bar import NavigationBar

# from pages.cockpit_page import CockpitPage
# from pages.login_page import LoginPage
# from pages.panel_page import PanelPage
project_name = "Nowyprojekt1"
@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    project_page = ProjectPage(browser)
    nav_bar = NavigationBar(browser)
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    project_page.load()
    project_page.add_project()
    project_page.fill_fields(project_name,Faker().last_name(),Faker().color())
    nav_bar.go_to_main_menu()
    project_page.load()
    project_page.fill_search_bar_and_execute(project_name)
    yield browser
    browser.quit()
def test_script_is_working(browser):
   status_of_project =  browser.find_element(By.CSS_SELECTOR,'.t_status').text
   project_name = browser.find_element(By.CSS_SELECTOR,'.t_number').text
   assert status_of_project == "Aktywny"
   assert  project_name == project_name
